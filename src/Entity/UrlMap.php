<?php

namespace App\Entity;

use App\Repository\UrlMapRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UrlMapRepository::class)
 */
class UrlMap
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $inputUrl;

    /**
     * @ORM\Column(type="text")
     */
    private $shortUrl;

    /**
     * @ORM\Column(type="integer")
     */
    private $hitCnt;

    /**
     * @ORM\Column(type="integer")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deactivate;


    //Getters and the Setters
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHitCnt()
    {
        return $this->hitCnt;
    }

    /**
     * @param mixed $hitCnt
     */
    public function setHitCnt($hitCnt): void
    {
        $this->hitCnt = $hitCnt;
    }

    /**
     * @return mixed
     */
    public function getDeactivate()
    {
        return $this->deactivate;
    }

    /**
     * @param mixed $deactivate
     */
    public function setDeactivate($deactivate): void
    {
        $this->deactivate = $deactivate;
    }

    /**
     * @return mixed
     */
    public function getInputUrl()
    {
        return $this->inputUrl;
    }

    /**
     * @param mixed $inputUrl
     */
    public function setInputUrl($inputUrl): void
    {
        $this->inputUrl = $inputUrl;
    }

    /**
     * @return mixed
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }

    /**
     * @param mixed $shortUrl
     */
    public function setShortUrl($shortUrl): void
    {
        $this->shortUrl = $shortUrl;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }



}
