<?php

namespace App\Repository;

use App\Entity\UrlMap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UrlMap|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlMap|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlMap[]    findAll()
 * @method UrlMap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlMapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UrlMap::class);
    }

    // /**
    //  * @return UrlMap[] Returns an array of UrlMap objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UrlMap
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
