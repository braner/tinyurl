<?php


namespace App\Services;


use App\Entity\UrlMap;
use Doctrine\ORM\EntityManagerInterface;

class Validator
{

    public function validURL($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if($httpcode>=200 && $httpcode<400){
            return true;
        }else{
            return false;
        }
    }
    public function validateUniqueToUser($userId, $url, EntityManagerInterface $entityManager) {
        $result = $entityManager->getRepository(UrlMap::class)->findBy(['user'=>$userId,'inputUrl' =>$url]);
        $bool = TRUE;
        $tinyurl = '';
        if (count($result) >= 1) {
            $bool = FALSE;
            $res = $result[0];
            $tinyurl = $res->getShortUrl();
        }
        $returnable = array($bool,$tinyurl);
        return $returnable;

    }
}