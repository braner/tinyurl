<?php


namespace App\Services;

use App\Entity\UrlMap;
use Doctrine\ORM\EntityManagerInterface;

class Redirects
{

    public function getLongUrl($url, EntityManagerInterface $entityManager)
    {
        return $entityManager->getRepository(UrlMap::class)->findBy(['shortUrl' => $url]);
    }

    public function increaseHitCnt($old, $ent, EntityManagerInterface $entityManager)
    {
        $new = $old + 1;
        $ent->setHitCnt($new);
        $entityManager->persist($ent);
        $entityManager->flush();
    }
}