<?php


namespace App\Services;


use App\Entity\UrlMap;
use App\Repository\UrlMapRepository;
use Doctrine\ORM\EntityManagerInterface;

class Admin
{

    public function getAllUrlsByUserId($user, EntityManagerInterface $entityManager)
    {
        return $entityManager->getRepository(UrlMap::class)->findBy(['user' => $user]);
    }

    public function createTinyUrl($url, $tinyUrl, $user, $hitcnt, $deactivate, EntityManagerInterface $entityManager)
    {
        $urlmap = new UrlMap();
        $urlmap->setInputUrl($url);
        $urlmap->setShortUrl($tinyUrl);
        $urlmap->setUser($user);
        $urlmap->setHitCnt($hitcnt);
        $urlmap->setDeactivate($deactivate);
        $entityManager->persist($urlmap);
        $entityManager->flush();
    }

    public function toggleActivation(String $url, String $action, UrlMapRepository $urlRepository, EntityManagerInterface $entityManager)
    {
        $entity = $urlRepository->findBy(['shortUrl' => $url])[0];

        if ($action == 'deactivate') {
            $entity->setDeactivate(1);
        } else {
            $entity->setDeactivate(0);
        }
        $entityManager->persist($entity);
        $entityManager->flush();
    }


}