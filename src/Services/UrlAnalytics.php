<?php


namespace App\Services;


use App\Entity\UrlMap;
use Doctrine\ORM\EntityManagerInterface;

class UrlAnalytics
{

    public function getUrlInfo($url, EntityManagerInterface $entityManager)
    {
        return $entityManager->getRepository(UrlMap::class)->findBy(['shortUrl' => $url]);
    }

}