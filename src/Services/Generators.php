<?php


namespace App\Services;


class Generators
{
    public function GenerateTinyUrl($url)
    {
        $randomSize = mt_rand(5,9);
        return substr(md5(uniqid($url)),0,$randomSize);
    }

}