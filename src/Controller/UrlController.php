<?php


namespace App\Controller;

use App\Repository\UrlMapRepository;
use App\Services\Admin;
use App\Services\Redirects;
use App\Services\UrlAnalytics;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Services\Validator;
use App\Services\Generators;

class UrlController extends AbstractController
{

    public function __construct(Validator $validator, Generators $generator, Admin $admin, Redirects $redirect, UrlAnalytics $urlAnalyticInfo)
    {

        $this->validator = $validator;
        $this->generator = $generator;
        $this->admin = $admin;
        $this->redirect = $redirect;
        $this->urlAnalyticInfo = $urlAnalyticInfo;

    }

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function index()
    {
        $params = [
            'Section' => '1',
        ];
        return $this->render('UrlShortener/index.html.twig', $params);
    }

    /**
     * @Route("/urlProcessor")
     * @Method({"GET"})
     */
    public function urlProcessor(Request $request, EntityManagerInterface $entityManager)
    {
        $reid = $request->request->all();
        $url = $reid['Url'];
        $user = $reid['user'];
        $message = 'This URL does not appear to be valid.';
        $passed = $this->validator->validURL($url);

        if ($passed) {
            $checkUnique = $this->validator->validateUniqueToUser($user, $url, $entityManager);
            $isNewUrl = $checkUnique[0];
        }

        if ($passed and $isNewUrl) {
            $tinyUrl = $this->generator->GenerateTinyUrl($url);
            $this->admin->createTinyUrl($url, $tinyUrl, $user, 0, 0, $entityManager);
            return $this->redirectToRoute("url_view", ['url' => $tinyUrl], 302);
        } else if ($passed) {
            $message = 'You already have a tiny Url for your submission. The tiny url is ' . $checkUnique[1];
        }
        $params = [
            'Section' => '2',
            'url' => $url,
            'user' => $user,
            'message' => $message
        ];

        return $this->render('UrlShortener/index.html.twig', $params);
    }


    /**
     * @Route("/urlAdmin")
     * @Method({"GET"})
     */
    public function urlAdminTool(Request $request, EntityManagerInterface $entityManager)
    {
        $message = 'Enter your UserId to retrieve all of your tiny urls.';
        $urlList = [];
        $reid = $request->request->all();
        if (count($reid) != 0) {
            $user = $reid['user'];
            if ($user == '') {
                $message = 'Enter your UserId to retrieve all of your tiny urls.';
            } else {
                $message = 'We did not find any urls for that user';
            }
            $urls = $this->admin->getAllUrlsByUserId($user, $entityManager);
            if (count($urls) != 0) {
                foreach ($urls as $url) {
                    $urlList[$url->getInputUrl()] = array($url->getShortUrl(), $url->getHitCnt(), $url->getDeactivate());;
                }
                $message = 'You have the following URLS.';
            }
        }
        $params = [
            'Section' => '3',
            'message' => $message,
            'lists' => $urlList,
            'ServerName' => $_ENV['APP_SERVER_HOST']
        ];
        return $this->render('UrlShortener/admin.html.twig', $params);
    }

    /**
     * @Route("/urlAnalytics")
     * @Method({"GET"})
     */

    public function urlAnalytics(Request $request, EntityManagerInterface $entityManager)
    {

        return $this->render('UrlShortener/analytic.html.twig');

    }

    /**
     * @Route("/urlAnalyticsResults")
     * @Method({"GET"})
     */

    public function urlAnalyticsResults(Request $request, EntityManagerInterface $entityManager): Response
    {

        $requestParams = $request->request->all();
        $url = $requestParams['url'];
        return $this->redirectToRoute("url_view", ['url' => $url], 302);

    }


    /**
     * @Route("/view/{url}", name="url_view")
     * @Method({"GET"})
     */

    public function urlAnalyticsView(string $url, Request $request, EntityManagerInterface $entityManager)
    {
        $urlInfo = $this->urlAnalyticInfo->getUrlInfo($url, $entityManager)[0];
        $params = [
            'longurl' => $urlInfo->getInputUrl(),
            'shorturl' => $urlInfo->getShortUrl(),
            'visitcount' => $urlInfo->getHitCnt(),
            'active' => $urlInfo->getDeactivate(),
            'ServerName' => $_ENV['APP_SERVER_HOST']
        ];

        return $this->render('UrlShortener/analyticresults.html.twig', $params);

    }


    /**
     * @Route("/{url}")
     * @Method({"GET"})
     */
    public function urlRedirect(Request $request, EntityManagerInterface $entityManager, $url)
    {

        //get redirect
        $longurl = $this->redirect->getLongUrl($url, $entityManager);
        $message = 'You entered ' . $url . ' and it does not exist or is not active';
        $active = $longurl[0]->getdeactivate();
        if (count($longurl) > 0 and $active == 0) {
            $redirecTarget = $longurl[0]->getInputUrl();
            $oldHitCnt = $longurl[0]->getHitCnt();
            $this->redirect->increaseHitCnt($oldHitCnt, $longurl[0], $entityManager);
            header("Location:" . $redirecTarget);
        }

        $params = [
            'message' => $message
        ];
        return $this->render('UrlShortener/notFound.html.twig', $params);

    }


    /**
     * @Route("/{url}/toggleActivation/{action}")
     * @Method({"POST"})
     */
    public function toggleUrlActivation(string $url, string $action, UrlMapRepository $urlRepository, EntityManagerInterface $entityManager)
    {

        $this->admin->toggleActivation( $url,$action, $urlRepository, $entityManager);

        return $this->redirectToRoute("url_view", ['url' => $url], 302);

    }

}


