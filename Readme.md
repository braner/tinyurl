# Tiny Url Project

This is a project to create tiny urls out off a longer URL

## Installation

You will need Composer please follow their documentation.
https://getcomposer.org/

after you have composer installed on your machine, go to your terminal and run the following command from your project root.



```bash
composer install
```

This will install all the dependencies of the project.

Next you will need to navigate to the .env file, and go to line 26, please update the database user, password, and name as you would like for example

in the following the first"root" is your user name, second root is your password.  TinySetup would be the name of the database.

DATABASE_URL="mysql://root:root@127.0.0.1:3307/TinySetup"

Next you will need to go back to the terminal and run the follow

```bash
 php bin/console  doctrine:database:create
```
that will create your Database

You will now be able to use the URL Shorten-er